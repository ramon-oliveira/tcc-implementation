OBJ_PATH=obj
SRC_PATH=src
SRCS=$(wildcard $(SRC_PATH)/*.cpp)
OBJS=$(patsubst %.cpp,$(OBJ_PATH)/%.o, $(notdir $(SRCS)))
CC=g++-4.9
CFLAGS=-std=c++11 -O2 -Wall
all: simulator

simulator: $(OBJS)
	$(CC) $(CFLAGS)  $^ -o $@

$(OBJ_PATH)/%.o : $(SRC_PATH)/%.cpp
	$(CC) $(CFLAGS) -c -o $@ $<

clean:
	-rm -f $(OBJ_PATH)/*.o simulator
