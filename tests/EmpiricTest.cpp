#include "EmpiricTest.h"

/****************************** PIGenerator tests *****************************/

Graph test1_PIGenerator(void){
    PIGenerator::Layer l1(1, 1, 1, 1);
    PIGenerator::Layer l2(2, 1, 1, 1);
    PIGenerator::Layer l3(4, 1, 1, 1);
    PIGenerator::Layer l4(8, 0, 1, 0);

    vector<PIGenerator::Layer> vl = {l1, l2, l3, l4};

    PIGenerator pigen(vl);

    Graph g = pigen.generate();

    cout << g;
    return g;
}

Graph test2_PIGenerator(void){
    PIGenerator::Layer l1(2, 1, 1, 1);
    PIGenerator::Layer l2(8, 1, 1, 1);
    PIGenerator::Layer l3(16, 1, 1, 1);
    PIGenerator::Layer l4(32, 0, 1, 0);

    vector<PIGenerator::Layer> vl = {l1, l2, l3, l4};

    PIGenerator pigen(vl);

    Graph g = pigen.generate();

    cout << g;
    return g;
}


/****************************** VIGenerator tests *****************************/

Graph test1_VIGenerator(void){
    srand(10);
    VIGenerator::Config config(
        PII(2, 10),
        PII(0, 1),
        {PII(1, 1), PII(1, 1)},
        PII(1, 1)
    );

    VIGenerator vigen(config);

    Graph g = vigen.generate();

    cout << g;
    return g;
}

Graph test2_VIGenerator(void){
    srand(123233);
    VIGenerator::Config config(
        PII(2, 10),
        PII(0, 1),
        {PII(1, 1), PII(1, 1)},
        PII(1, 1)
    );

    VIGenerator vigen(config);

    Graph g = vigen.generate();

    cout << g;
    return g;
}


void test_VITreeM(){
    srand(time(NULL));
    PIGenerator::Layer l1(8, 1, 100, 100);
    PIGenerator::Layer l2(16, 1, 100, 100);
    PIGenerator::Layer l3(32, 1, 100, 100);
    PIGenerator::Layer l4(96, 0, 100, 100);
    vector<PIGenerator::Layer> vl = {l1, l2, l3, l4};
    PIGenerator pigen(vl);
    Graph phy = pigen.generate();
    Tree tphy(phy, Tree::Converter::PHYSICAL, Tree::Root::CENTER);
    cout << "thpy root: " << tphy.root << "\n";

    VIGenerator::Config config(
        PII(2, 10),
        PII(0, 1),
        {PII(1, 10), PII(1, 10)},
        PII(1, 10)
    );
    VIGenerator vigen(config);

    Graph vir = vigen.generate();
    Tree tvir(vir, Tree::Converter::VIRTUAL, Tree::Root::CENTER);
    cout << "Graph:\n";
    cout << vir << "\n\n\n";
    cout << "Tree:\n";
    cout << tvir << "\n";

    VITreeM mapp(tphy, tvir, VITreeM::Selection::BEST_FIT);
    if(mapp.is_valid())
        cout << "success\n";
    else
        cout << "fail\n";
}

void test_GraphGen(){
    srand(time(NULL));

    GraphGen::Layer l1(PII(1, 1), 1, PII(50,100), PII(50,100));
    GraphGen::Layer l2(PII(2, 2), 1, PII(50,100), PII(50,100));
    GraphGen::Layer l3(PII(4, 4), 1, PII(50,100), PII(50,100));
    GraphGen::Layer l4(PII(8, 8), 0, PII(50,100), PII(50,100));
    vector<GraphGen::Layer> vl = {l1, l2, l3, l4};
    GraphGen gen(GraphGen::Config(PII(2,4), vl));

    cout << gen.upper_gen() << "\n\n\n";

    cout << gen.random_gen();


}

