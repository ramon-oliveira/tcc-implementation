#ifndef __EMPIRIC_TEST__
#define __EMPIRIC_TEST__

#include "StdLib.h"
#include "PIGenerator.h"
#include "VIGenerator.h"
#include "Tree.h"
#include "PIManager.h"
#include "GraphGen.h"

/* PIGenerator tests */
Graph test1_PIGenerator(void); // OK
Graph test2_PIGenerator(void); // OK

/* VIGenerator tests */
Graph test1_VIGenerator(void); // OK
Graph test2_VIGenerator(void); // OK

void test_VITreeM();

void test_GraphGen();
#endif
