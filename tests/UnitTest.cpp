#include "Catch.h"

#include "Node.h"
#include "Edge.h"
#include "Graph.h"
#include "SuperNode.h"
#include "VITreeM.h"
#include "Utils.h"
#include "PIManager.h"

void tests(){
    //int result = Catch::Session().run(argc, argv);
}

TEST_CASE("Node test", "[node]"){
    Node n1(0, 0, 1);
    REQUIRE(n1.id == 0);
    REQUIRE(n1.type == 0);
    REQUIRE(n1.cap == 1);

    Node n2(1, 0, 10);

    SECTION("< operator"){
        REQUIRE(n1 < n2);
    }

    SECTION("+= operator"){
        n1 += n2;
        REQUIRE(n1.cap == 11);

        n2 += n1;
        REQUIRE(n2.cap == 21);
    }

    SECTION("-= operator"){
        n2 -= n1;
        REQUIRE(n2.cap == 9);
    }
}

TEST_CASE("Edge test","[edge]"){
    Edge a(2, 1, 3);
    
    REQUIRE(a.u == 1);
    REQUIRE(a.v == 2);
    REQUIRE(a.cap == 3);
    
    SECTION("== operator"){
        Edge b(1, 2, 3);
        REQUIRE(a == b);

        b.cap = -1;
        REQUIRE(a == b);
    }

    SECTION("< operator"){
        Edge b(2, 3, 3);
        REQUIRE(a < b);
    }
}

TEST_CASE("Graph test", "[graph]"){
    vector<Node> nodes = {
        Node(0, 1, 1),
        Node(1, 1, 1),
        Node(2, 1, 1),
        Node(3, 1, 1)
    };

    vector<Edge> edges = {
        Edge(0, 1, 1),
        Edge(0, 3, 1),
        Edge(1, 2, 1)
    };

    Graph g(nodes, edges);

    REQUIRE(g.adj_list.size() == 4);

    REQUIRE(g.adj_list[0].size() == 2);
    REQUIRE(g.adj_list[0][0] == 1);
    REQUIRE(g.adj_list[0][1] == 3);

    REQUIRE(g.adj_list[1].size() == 2);
    REQUIRE(g.adj_list[1][0] == 0);
    REQUIRE(g.adj_list[1][1] == 2);

    REQUIRE(g.adj_list[2].size() == 1);
    REQUIRE(g.adj_list[2][0] == 1);

    REQUIRE(g.adj_list[3].size() == 1);
    REQUIRE(g.adj_list[3][1] == 0);

    int n = g.nodes.size();
    for(int u=0; u<n; u++){
        for(int v=0; v<n; v++){
            if(g.cap_mtx[u][v] > 0){
                REQUIRE(find(all(g.adj_list[u]), v) != g.adj_list[u].end());
            }else{
                REQUIRE(find(all(g.adj_list[u]), v) == g.adj_list[u].end());
            }
        }
    }        
   

    SECTION("connected"){
        REQUIRE(g.connected() == true);

        g.remove_edge(Edge(0, 1, -1));
        REQUIRE(g.connected() == false);
    } 

    SECTION("add_edge"){
        g.add_edge(Edge(2, 3, 1));
        
        REQUIRE(g.cap_mtx[2][3] == 1);
        REQUIRE(g.cap_mtx[3][2] == 1);

        REQUIRE(g.adj_list[2][1] == 3);
        REQUIRE(g.adj_list[3][1] == 2);
    }
    
    SECTION("remove_edge"){
        g.remove_edge(Edge(0, 1, -1));
        
        REQUIRE(g.cap_mtx[0][1] == 0);
        REQUIRE(g.cap_mtx[1][0] == 0);

        REQUIRE(find(all(g.adj_list[0]), 1) == g.adj_list[0].end());
        REQUIRE(find(all(g.adj_list[1]), 0) == g.adj_list[1].end());
    }
}

TEST_CASE("SuperNode test", "[super_node]"){
    Node n1(0, 0, 1); 
    Node n2(1, 1, 1); 

    SuperNode sn1(n1);
    REQUIRE(sn1.node_id == 0);
    REQUIRE(sn1.edge_cap == 0);
    REQUIRE(sn1.node_types_cap.size() == 1);
    REQUIRE(sn1.node_types_cap[0] == 1);

    SuperNode sn2(n2);

    SECTION("+= operator"){
        sn1 += sn2;
        REQUIRE(sn1.node_types_cap.size() == 2);
        REQUIRE(sn1.node_types_cap[0] == 1);
        REQUIRE(sn1.node_types_cap[1] == 1);
    }

    SECTION("-= operator"){
        sn1 += sn2;
        sn1 -= sn2;

        REQUIRE(sn1.node_types_cap.size() == 1);
        REQUIRE(sn1.node_types_cap[0] == 1);
    }

    SECTION(">= operator"){
        sn1 += sn2;
        REQUIRE(sn1 >= sn2);
    }

    SECTION("< operator"){
        sn1 += sn2;
        sn1 += sn1;
        sn1.edge_cap += 1;
        REQUIRE(sn2 < sn1);
    }

    SECTION("raw_cost"){
        sn1 += sn2;
        REQUIRE(sn1.raw_cost() == 2);
    }
}

TEST_CASE("Tree test1", "[tree]"){
    vector<Node> nodes = {
        Node(0, 1, 1),
        Node(1, 1, 1),
        Node(2, 1, 1),
        Node(3, 1, 1),
        Node(4, 1, 1)
    };

    vector<Edge> edges = {
        Edge(0, 1, 1),
        Edge(0, 2, 1),
        Edge(1, 2, 1),
        Edge(1, 3, 1),
        Edge(2, 4, 1)
    };

    Graph g(nodes, edges);
    
    Tree t(g, Tree::VIRTUAL, Tree::LRC);

    REQUIRE(t.root == 0);

    REQUIRE(t.adj_list[0].size() == 2);

    REQUIRE(t.virtual_adj_list[1][0] == 2);
    REQUIRE(t.virtual_adj_list[2][0] == 1);

    REQUIRE(t.cap_mtx[0][1] == 2);
    REQUIRE(t.cap_mtx[0][2] == 2);

    t.add_to_edge(0, 2, 1);
    REQUIRE(t.cap_mtx[0][2] == 3);
}

TEST_CASE("Tree test2", "[tree]"){
    vector<Node> pnodes = {
        Node(0, 1, 1),
        Node(1, 1, 1),
        Node(2, 1, 1),
        Node(3, 0, 1),
        Node(4, 0, 1),
        Node(5, 0, 1),
        Node(6, 0, 1),
        Node(7, 0, 1),
        Node(7, 0, 1),
        Node(9, 0, 1),
        Node(10, 0, 1)
    };

    vector<Edge> pedges = {
        Edge(0, 1, 1),
        Edge(0, 2, 1),
        Edge(1, 3, 1),
        Edge(1, 4, 1),
        Edge(1, 5, 1),
        Edge(1, 6, 1),
        Edge(2, 7, 1),
        Edge(2, 8, 1),
        Edge(2, 9, 1),
        Edge(2, 10, 1),
    };

    Graph phy(pnodes, pedges);
    Tree tphy(phy, Tree::PHYSICAL, Tree::LRC);

    SECTION("Root must be the heaviest node"){
        REQUIRE(tphy.root == 0);
    }

    SECTION("Virtual edges"){
        for(int u=0; u<(int)phy.nodes.size(); u++){
            REQUIRE(tphy.virtual_adj_list[u].size() == 0);
        }
    }

    SECTION("cap_mtx must be equal to graph.cap_mtx"){
        REQUIRE(tphy.cap_mtx == phy.cap_mtx);    
    }

    SECTION("Super nodes"){
        SuperNode sn00;
        sn00.node_id = 0;
        sn00.edge_cap = 10;
        sn00.node_types_cap[0] = 8;
        sn00.node_types_cap[1] = 3;
        REQUIRE(sn00 == tphy.super_nodes[0][0]);
        
        SuperNode sn01; 
        sn01.node_id = 0;
        sn01.edge_cap = 4;
        sn01.node_types_cap[0] = 4;
        sn01.node_types_cap[1] = 1;
        REQUIRE(sn01 == tphy.super_nodes[0][1]);

        SuperNode sn10; 
        sn10.node_id = 1;
        sn10.edge_cap = 5;
        sn10.node_types_cap[0] = 4;
        sn10.node_types_cap[1] = 2;
        REQUIRE(sn10 == tphy.super_nodes[1][0]);

        SuperNode sn31; 
        sn31.node_id = 3;
        sn31.edge_cap = 9;
        sn31.node_types_cap[0] = 7;
        sn31.node_types_cap[1] = 3;
        REQUIRE(sn31 == tphy.super_nodes[3][1]);
    }
}

TEST_CASE("VITreeM test (without cycle)", "[vitreem]"){
    vector<Node> pnodes = {
        Node(0, 1, 1),
        Node(1, 1, 1),
        Node(2, 1, 1),
        Node(3, 0, 1),
        Node(4, 0, 1),
        Node(5, 0, 1),
        Node(6, 0, 1),
        Node(7, 0, 1),
        Node(7, 0, 1),
        Node(9, 0, 1),
        Node(10, 0, 1)
    };

    vector<Edge> pedges = {
        Edge(0, 1, 2),
        Edge(0, 2, 1),
        Edge(1, 3, 1),
        Edge(1, 4, 1),
        Edge(1, 5, 1),
        Edge(1, 6, 1),
        Edge(2, 7, 1),
        Edge(2, 8, 1),
        Edge(2, 9, 1),
        Edge(2, 10, 1),
    };

    Graph phy(pnodes, pedges);
    Tree tphy(phy, Tree::PHYSICAL, Tree::LRC);


    SECTION("Virtual 1"){
        vector<Node> vnodes = {
            Node(0, 1, 1),
            Node(1, 0, 1),
            Node(2, 0, 1),
        };

        vector<Edge> vedges = {
            Edge(0, 1, 1),
            Edge(0, 2, 1),
        };

        Graph vir(vnodes, vedges);
        Tree tvir(vir, Tree::VIRTUAL, Tree::LRC);

        VITreeM vitreem(tphy, tvir, VITreeM::BEST_FIT);

        VI expected = {1, 3, 4};
        REQUIRE(vitreem.nodes_map == expected);
    }

    SECTION("Virtual 2"){
        vector<Node> vnodes = {
            Node(0, 1, 1),
            Node(3, 1, 1),
            Node(2, 0, 1),
            Node(1, 0, 1),
        };

        vector<Edge> vedges = {
            Edge(0, 1, 1),
            Edge(0, 2, 1),
            Edge(0, 3, 1),
        };

        Graph vir(vnodes, vedges);
        Tree tvir(vir, Tree::VIRTUAL, Tree::LRC);

        VITreeM vitreem(tphy, tvir, VITreeM::BEST_FIT);

        VI expected = {0, 3, 4, 2};
        REQUIRE(vitreem.nodes_map == expected);
    }

    SECTION("Virtual 3"){
        vector<Node> vnodes = {
            Node(0, 1, 1),
            Node(1, 0, 1),
            Node(2, 0, 1),
            Node(3, 0, 1),
            Node(4, 0, 1),
        };
        
        vector<Edge> vedges = {
            Edge(0, 1, 1),
            Edge(0, 2, 1),
            Edge(0, 3, 1),
            Edge(0, 4, 1),
        };

        Graph vir(vnodes, vedges);
        Tree tvir(vir, Tree::VIRTUAL, Tree::LRC);

        VITreeM vitreem(tphy, tvir, VITreeM::BEST_FIT);

        VI expected = {1, 3, 4, 5, 6};
        REQUIRE(vitreem.nodes_map == expected);
    }

    SECTION("Virtual 4"){
        vector<Node> vnodes = {
            Node(0, 1, 1),
            Node(1, 1, 1),
            Node(2, 1, 1),
            Node(3, 0, 1),
            Node(4, 0, 1),
            Node(5, 0, 1),
        };
        
        vector<Edge> vedges = {
            Edge(0, 1, 1),
            Edge(0, 2, 1),
            Edge(1, 3, 1),
            Edge(1, 4, 1),
            Edge(2, 5, 1),
        };

        Graph vir(vnodes, vedges);
        Tree tvir(vir, Tree::VIRTUAL, Tree::LRC);

        VITreeM vitreem(tphy, tvir, VITreeM::BEST_FIT);

        VI expected = {0, 1, 2, 3, 4, 7};
        REQUIRE(vitreem.nodes_map == expected);
    }
}

TEST_CASE("VITreeM test (with cycle)", "[vitreem]"){
    vector<Node> pnodes = {
        Node(0, 1, 1),
        Node(1, 1, 1),
        Node(2, 1, 1),
        Node(3, 0, 1),
        Node(4, 0, 1),
        Node(5, 0, 1),
        Node(6, 0, 1),
        Node(7, 0, 1),
        Node(7, 0, 1),
        Node(9, 0, 1),
        Node(10, 0, 1)
    };

    vector<Edge> pedges = {
        Edge(0, 1, 2),
        Edge(0, 2, 1),
        Edge(1, 2, 1),
        Edge(1, 3, 1),
        Edge(1, 4, 1),
        Edge(1, 5, 1),
        Edge(1, 6, 1),
        Edge(2, 7, 1),
        Edge(2, 8, 1),
        Edge(2, 9, 1),
        Edge(2, 10, 1),
    };

    Graph phy(pnodes, pedges);
    Tree tphy(phy, Tree::PHYSICAL, Tree::LRC);


    SECTION("Virtual 1"){
        vector<Node> vnodes = {
            Node(0, 1, 1),
            Node(1, 0, 1),
            Node(2, 0, 1),
        };

        vector<Edge> vedges = {
            Edge(0, 1, 1),
            Edge(0, 2, 1),
        };

        Graph vir(vnodes, vedges);
        Tree tvir(vir, Tree::VIRTUAL, Tree::LRC);

        VITreeM vitreem(tphy, tvir, VITreeM::BEST_FIT);

        VI expected = {1, 3, 4};
        REQUIRE(vitreem.nodes_map == expected);
    }

    SECTION("Virtual 2"){
        vector<Node> vnodes = {
            Node(0, 1, 1),
            Node(3, 1, 1),
            Node(2, 0, 1),
            Node(1, 0, 1),
        };

        vector<Edge> vedges = {
            Edge(0, 1, 1),
            Edge(0, 2, 1),
            Edge(0, 3, 1),
        };

        Graph vir(vnodes, vedges);
        Tree tvir(vir, Tree::VIRTUAL, Tree::LRC);

        VITreeM vitreem(tphy, tvir, VITreeM::BEST_FIT);

        VI expected = {0, 3, 4, 2};
        REQUIRE(vitreem.nodes_map == expected);
    }

    SECTION("Virtual 3"){
        vector<Node> vnodes = {
            Node(0, 1, 1),
            Node(1, 0, 1),
            Node(2, 0, 1),
            Node(3, 0, 1),
            Node(4, 0, 1),
        };
        
        vector<Edge> vedges = {
            Edge(0, 1, 1),
            Edge(0, 2, 1),
            Edge(0, 3, 1),
            Edge(0, 4, 1),
        };

        Graph vir(vnodes, vedges);
        Tree tvir(vir, Tree::VIRTUAL, Tree::LRC);

        VITreeM vitreem(tphy, tvir, VITreeM::BEST_FIT);

        VI expected = {1, 3, 4, 5, 6};
        REQUIRE(vitreem.nodes_map == expected);
    }

    SECTION("Virtual 4"){
        vector<Node> vnodes = {
            Node(0, 1, 1),
            Node(1, 1, 1),
            Node(2, 1, 1),
            Node(3, 0, 1),
            Node(4, 0, 1),
            Node(5, 0, 1),
        };
        
        vector<Edge> vedges = {
            Edge(0, 1, 1),
            Edge(0, 2, 1),
            Edge(1, 3, 1),
            Edge(1, 4, 1),
            Edge(2, 5, 1),
        };

        Graph vir(vnodes, vedges);
        Tree tvir(vir, Tree::VIRTUAL, Tree::LRC);

        VITreeM vitreem(tphy, tvir, VITreeM::BEST_FIT);

        VI expected = {0, 1, 2, 3, 4, 7};
        REQUIRE(vitreem.nodes_map == expected);
    }
}


TEST_CASE("PIManager test", "[pimanager]"){
    vector<Node> pnodes = {
        Node(0, 1, 2),
        Node(1, 1, 2),
        Node(2, 1, 2),
        Node(3, 0, 1),
        Node(4, 0, 1),
        Node(5, 0, 1),
        Node(6, 0, 1),
        Node(7, 0, 1),
        Node(7, 0, 1),
        Node(9, 0, 1),
        Node(10, 0, 1)
    };

    vector<Edge> pedges = {
        Edge(0, 1, 2),
        Edge(0, 2, 2),
        Edge(1, 3, 1),
        Edge(1, 4, 1),
        Edge(1, 5, 1),
        Edge(1, 6, 1),
        Edge(2, 7, 1),
        Edge(2, 8, 1),
        Edge(2, 9, 1),
        Edge(2, 10, 1),
    };

    Graph phy(pnodes, pedges);
    PIManager pimanager(phy, Tree::LRC, VITreeM::BEST_FIT);


    SECTION("Virtual 1"){
        vector<Node> vnodes = {
            Node(0, 1, 1),
            Node(1, 0, 1),
            Node(2, 0, 1),
        };

        vector<Edge> vedges = {
            Edge(0, 1, 1),
            Edge(0, 2, 1),
        };

        Graph vir(vnodes, vedges);

        pimanager.allocate(0, vir); 
        pimanager.allocate(1, vir); 
        pimanager.allocate(2, vir); 
        pimanager.deallocate(0, vir);
        pimanager.deallocate(1, vir);
        pimanager.deallocate(2, vir);

        REQUIRE(pimanager.phy == phy);
    }

    SECTION("Virtual 2"){
        vector<Node> vnodes = {
            Node(0, 1, 1),
            Node(3, 1, 1),
            Node(2, 0, 1),
            Node(1, 0, 1),
        };

        vector<Edge> vedges = {
            Edge(0, 1, 1),
            Edge(0, 2, 1),
            Edge(0, 3, 1),
        };

        Graph vir(vnodes, vedges);

        pimanager.allocate(0, vir);
        pimanager.allocate(1, vir);
        pimanager.deallocate(0, vir);
        pimanager.deallocate(1, vir);
        
        REQUIRE(phy == pimanager.phy);
    }

    SECTION("Virtual 3"){
        vector<Node> vnodes = {
            Node(0, 1, 1),
            Node(1, 0, 1),
            Node(2, 0, 1),
            Node(3, 0, 1),
            Node(4, 0, 1),
        };
        
        vector<Edge> vedges = {
            Edge(0, 1, 1),
            Edge(0, 2, 1),
            Edge(0, 3, 1),
            Edge(0, 4, 1),
        };

        Graph vir(vnodes, vedges);

        pimanager.allocate(0, vir);
        pimanager.allocate(1, vir);
        pimanager.deallocate(0, vir);
        pimanager.deallocate(1, vir);

        REQUIRE(phy == pimanager.phy);
    }

    SECTION("Virtual 4"){
        vector<Node> vnodes = {
            Node(0, 1, 1),
            Node(1, 1, 1),
            Node(2, 1, 1),
            Node(3, 0, 1),
            Node(4, 0, 1),
            Node(5, 0, 1),
        };
        
        vector<Edge> vedges = {
            Edge(0, 1, 1),
            Edge(0, 2, 1),
            Edge(1, 3, 1),
            Edge(1, 4, 1),
            Edge(2, 5, 1),
        };

        Graph vir(vnodes, vedges);
        
        pimanager.allocate(0, vir);
        pimanager.allocate(1, vir);
        pimanager.deallocate(0, vir);
        
        REQUIRE(phy == pimanager.phy);
    } 
}
