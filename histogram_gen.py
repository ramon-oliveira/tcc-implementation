#!/usr/bin/env python
# coding: utf-8

import matplotlib.pyplot as pyplot
import json
import sys
import math


dictionary = {
    "Acceptance": u"Aceitação",
    "Mapping time": u"Tempo de mapeamento",
    "Fragmentation": u"Fragmentação",
    "Channel load": u"Carga dos canais",
    "Interval": u"Intervalo",
    "interval": u"Intervalo",
    "CENTER": "CENTRO",
}


def translate(txt):
    for d in dictionary:
        if d in txt:
            return txt.replace(d, dictionary[d])
    return txt


markers = ['.', 'o', '^', '1', 's', 'p', '*', '+', 'x', 'D', 'h', ',']

colors = [
    (0, 255, 0),
    (0, 0, 255),
    (255, 0, 0),
    (80, 80, 80),
    (0, 0, 255),
    (192, 192, 192),
    (128, 128, 128),
    (128, 0, 0),
    (128, 128, 0),
    (0, 128, 0),
    (128, 0, 128),
    (0, 128, 128),
    (0, 0, 128)
]

colors = [(1.0*r/255, 1.0*g/255, 1.0*b/255) for r, g, b in colors]


def graphics_gen(jgraphic, filename):
    """Graphic bar generator."""
    pyplot.figure(figsize=(20, 15))

    bar_width = 0.35
    opacity = 0.8
    error_config = {'ecolor': '0.3'}

    description = jgraphic["description"]
    pyplot.suptitle(description)

    nplots = len(jgraphic["subgraphics"])
    curplot = 1
    for subgraphic in jgraphic["subgraphics"]:
        if "ver" in subgraphic["title"]:
            continue

        ax = pyplot.subplot(nplots//2 + nplots % 2, 2, curplot)
        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width*0.8, box.height])

        cnt = 0
        for i, curve in enumerate(subgraphic["curves"]):
            name = curve["name"]
            if r"%" in name:
                continue
            name = name[:-8]
            cnt += 1
            ys = [y for x, y in curve["points"]]
            n = len(ys)
            mean = sum(ys)/n
            std = math.sqrt(sum((x-mean)**2 for x in ys)/n)

            pyplot.bar([cnt], [mean], bar_width,
                       alpha=opacity,
                       color=colors[cnt-1],
                       yerr=[std],
                       error_kw=error_config,
                       label=translate(name))

        pyplot.setp(ax.get_xticklabels(), visible=False)
        pyplot.title(translate(subgraphic["title"]), fontsize=20)
        pyplot.ylabel(translate(subgraphic["ylabel"]), fontsize=20)
        pyplot.xlim(xmin=0, xmax=cnt+1)
        if "axis" in subgraphic:
            pyplot.ylim(subgraphic["axis"][2:])
        elif "time" in subgraphic["title"]:
            print("hehe")
            pyplot.ylim((0, 4))
        pyplot.legend(loc="upper left", fontsize=20)
        curplot += 1

    pyplot.savefig(filename)

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print 'usage graphics_gen <infilename> <outfilename>'
    else:
        infilename = sys.argv[1]
        outfilename = sys.argv[2]
        with open(infilename, 'r') as f:
            gplot = json.loads(f.read())
            graphics_gen(gplot, outfilename)
