#include "PIGraphGen.h"

PIGraphGen::PIGraphGen(PIGraphGen::Config config):
    config(config)
{}

void PIGraphGen::add_datacenter(
        vector<Node> &nodes,
        vector<Edge> &edges,
        VI &core_ids
){
    int id = nodes.size();

    PII core(id, id+1);
    core_ids.push_back(id);
    core_ids.push_back(id+1);

    PII agregation(core.second+1, core.second+1+3);
    PII tor(agregation.second+1,agregation.second+1+3);
    PII server(tor.second+1, tor.second+1+47);

    for(int cid=core.first; cid<=core.second; cid++)
        nodes.push_back(Node(cid, 1, config.dc.core_node_cap));

    for(int cid=agregation.first; cid<=agregation.second; cid++)
        nodes.push_back(Node(cid, 1, config.dc.agregation_node_cap));

    for(int cid=tor.first; cid<=tor.second; cid++)
        nodes.push_back(Node(cid, 1, config.dc.tor_node_cap));

    for(int cid=server.first; cid<=server.second; cid++)
        nodes.push_back(Node(cid, 0, config.dc.server_node_cap));


    // core->agregation edges
    for(int cid=core.first; cid<=core.second; cid++)
        for(int aid=agregation.first; aid<=agregation.second; aid++)
            edges.push_back(Edge(cid, aid, config.dc.core_edge_cap));

    // agregation->tor edges
    for(int aid=agregation.first; aid<=agregation.second; aid++){
        int tini, tend;
        if(aid <= agregation.first+1){
            tini = tor.first;
            tend = tor.first+1;
        }else{
            tini = tor.first+2;
            tend = tor.second;
        }

        for(int tid=tini; tid<=tend; tid++)
            edges.push_back(Edge(aid, tid, config.dc.agregation_edge_cap));
    }

    // tor->server edges
    for(int tid=tor.first, k=0; tid<=tor.second; tid++, k++){
        int sini = server.first + k*12;
        int send = sini + 11;
        for(int sid=sini; sid<=send; sid++)
            edges.push_back(Edge(tid, sid, config.dc.tor_edge_cap));
    }
}


Graph PIGraphGen::generate(){
    vector<Node> nodes;
    vector<Edge> edges;
    VI core_ids;

    for(int i=0; i<config.n_dc; i++)
        add_datacenter(nodes, edges, core_ids);

    for(int i=0; i<(int)core_ids.size(); i+=2){
        int cid1 = core_ids[i];
        for(int j=i+2; j<(int)core_ids.size(); j++){
            int cid2 = core_ids[j];
            edges.push_back(Edge(cid1, cid2, config.dc.core_edge_cap));
        }

        cid1 = core_ids[i+1];
        for(int j=i+2; j<(int)core_ids.size(); j++){
            int cid2 = core_ids[j];
            edges.push_back(Edge(cid1, cid2, config.dc.core_edge_cap));
        }
    }

    return Graph(nodes, edges);
}