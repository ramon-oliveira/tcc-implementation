#ifndef __SIMULATOR__
#define __SIMULATOR__

#include "StdLib.h"
#include "Utils.h"
#include "JSON.h"
#include "GraphGen.h"
#include "PIGraphGen.h"
#include "FTGraphGen.h"
#include "PIManager.h"

class Simulator{
public:	

	struct Event{
		static const int IN = 1;
		static const int OUT = 0;
		
		int vid; // vi id
		int interval;
		int type; // IN: 1, OUT: 0 
		
		Event(int _vid, int _interval, int _type):
			vid(_vid),
			interval(_interval),
			type(_type)
		{}
		
		bool operator<(const Event &other) const {
			if(interval != other.interval)
				return interval < other.interval;
			return vid < other.vid;
		}
	};

    double total, current;

	void run(GraphGen *pigen, GraphGen *vigen, int nvi, PII interval,
            string out_filename);

    map<string, vector<PDD> > simulate(
        Graph phy, 
        vector<Graph> vis, 
        vector<Event> events,
        Tree::Root root_type,
        VITreeM::Selection selection_type,
        double alpha
    );

};

JSONObject create_subgraphic(
    wstring title,
    wstring xlabel,
    wstring ylabel,
    PII xaxis = PII(-1, -1),
    PII yaxis = PII(-1, -1)
);

#endif
