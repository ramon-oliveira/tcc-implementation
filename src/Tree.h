#ifndef __TREE__
#define __TREE__

#include "StdLib.h"
#include "Graph.h"
#include "SuperNode.h"
#include "Utils.h"

class Tree{
public:
    enum Converter{ VIRTUAL, PHYSICAL };
    enum Root{ LRC, CENTER };

	Tree(Graph _graph, Converter converter_type, Root root_type);
	Tree();
	
	Graph graph;
	vector< map<int, SuperNode> > super_nodes;
	VVI adj_list;
	VVI virtual_adj_list;
    VVI cap_mtx;
    int root;

    void add_to_edge(int a, int b, int value);
    VI path_btw(int a, int b);
    SuperNode get_box(int from, int to);
private:
    void converter(Converter converter_type);

    void kruskal();

	void build();	
	SuperNode build_dfs(int parent, int current);



    void define_root(Root root_type);
    void root_lrc();
    void root_center();
};

ostream &operator<<(ostream &os, const Tree &t);

#endif
