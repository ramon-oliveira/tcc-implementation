#include "FMGraphGen.h"


FMGraphGen::FMGraphGen(Config config):
    config(config)
{}

Graph FMGraphGen::generate(){
    vector<Node> nodes;
    vector<Edge> edges;

    int n_nodes = rand_btw(config.n_nodes);

    for(int u=0; u<n_nodes; u++)
        nodes.push_back(Node(u, rand_btw(0, 1), rand_btw(config.node_cap)));
    
    Graph graph(nodes, edges);
    
    for(int u = 0; u<n_nodes; u++)
        for(int v = u+1; v<n_nodes; v++)
            if(rand_btw(0, 1) == 1)
                graph.add_edge(Edge(u, v, rand_btw(config.edge_cap)));

    while(not graph.connected()){
        int u = rand_btw(0, n_nodes-1);
        int v = rand_btw(0, n_nodes-1);
        graph.add_edge(Edge(u, v, rand_btw(config.edge_cap)));
    }
    
    return graph; 
}
