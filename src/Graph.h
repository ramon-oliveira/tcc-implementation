#ifndef __GRAPH__
#define __GRAPH__

#include "StdLib.h"

#include "Edge.h"
#include "Node.h"

class Graph{
public:
	Graph(vector<Node> _nodes, vector<Edge> _edges);
    Graph();

	vector<Node> nodes; // list of nodes
	VVI adj_list; // adjacence list (node)
    VVI cap_mtx; // capacities matrix [node][node]

    bool connected();
    void add_edge(Edge e);
    void remove_edge(Edge e);
    bool operator==(const Graph &other) const;
private:
    void connected_dfs(int u, VI &vis);
};

ostream &operator<<(ostream &os, const Graph &g);

#endif
