#ifndef __NODE__
#define __NODE__

#include "StdLib.h"

class Node{
public:
	Node(int id, int type, int capacity);

	void operator-=(const Node &other);
	void operator+=(const Node &other);

	bool operator<(const Node &other) const;

	int id; //node identifier (must be unique in same graph)
	int type; //type of node (vm, router, ...)
	int cap; //node capacity
private:	
};

ostream &operator<<(ostream &os, const Node &n);

#endif
