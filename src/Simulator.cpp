#include "Simulator.h"

wstring to_wstring(string s){
    return wstring(all(s));
}

void Simulator::run(
        GraphGen *pigen, 
        GraphGen *vigen,
        int n_vis,
        PII interval,
        string out_filename
){
    vector<Graph> vis;
    vector<Event> events;
    
    Graph phy = pigen->generate();
    int phy_edges = 0;
    int phy_nodes = phy.nodes.size();
    for(int u=0; u<(int)phy.nodes.size(); u++)
        phy_edges += phy.adj_list[u].size();
    phy_edges /= 2;

    // gen nvi virtual infrastructure
    int vir_nodes = 0;
    int vir_edges = 0;
    for(int vid=0; vid<n_vis; vid++){
        vis.push_back(vigen->generate());
        vir_nodes += vis[vid].nodes.size();
        int ecount = 0;
        for(int u=0; u<(int)vis[vid].nodes.size(); u++)
            ecount += vis[vid].adj_list[u].size();
        vir_edges += ecount/2;
        int delta = rand_btw(1, 30);
        int ini = rand_btw(1, interval.second - delta);
        int end = rand_btw(ini + 1, ini + delta);
        events.push_back(Event(vid, ini, 1)); // in event
        events.push_back(Event(vid, end, 0)); // out event
    }

    vir_nodes /= n_vis;
    vir_edges /= n_vis;

    sort(all(events));

    wstring description = L"";
    description += L"Simulation: [n_vis :" + to_wstring(n_vis) + L"]";
    description += L"[interval: " + to_wstring(interval.first) + L"-";
    description += to_wstring(interval.second) + L"]\n\n";

    description += L"Physical: ";
    description += L"[nodes: " + to_wstring(phy_nodes) + L"]";
    description += L"[edges: " + to_wstring(phy_edges) + L"]\n";

    description += L"Virtual (average): ";
    description += L"[nodes: " + to_wstring(vir_nodes) + L"]";
    description += L"[edges: " + to_wstring(vir_edges) + L"]";

    JSONObject root;
    root[L"description"] = new JSONValue(description);

    JSONObject jtime_mapping;
    jtime_mapping = create_subgraphic(L"Tempo de mapeamento", L"periodo",
            L"Tempo de mapeamento (s)");

    JSONObject jfragmentation;
    jfragmentation = create_subgraphic(L"Fragmentação", L"periodo",
            L"Fragmentação (%)", interval, PII(0, 110));

    JSONObject jacceptance;
    jacceptance = create_subgraphic(L"Aceitação", L"periodo",
            L"Aceitação (%)", interval, PII(0, 110));

    JSONObject jovercost;
    jovercost = create_subgraphic(L"Sobrecusto", L"periodo",
            L"Sobrecusto (%)", interval, PII(0, 110));

    JSONObject jnodeload;
    jnodeload = create_subgraphic(L"Carga dos nós", L"periodo",
            L"Carga dos nós (%)", interval, PII(0, 110));

    JSONObject jlinkload;
    jlinkload = create_subgraphic(L"Carga dos canais", L"periodo",
            L"Carga dos canais (%)", interval, PII(0, 110));


    vector< tuple<Tree::Root, VITreeM::Selection, double> > params = {
        make_tuple(Tree::Root::LRC, VITreeM::Selection::BEST_FIT, 0.0),
        //make_tuple(Tree::Root::LRC, VITreeM::Selection::BEST_FIT, 0.50),
        //make_tuple(Tree::Root::LRC, VITreeM::Selection::BEST_FIT, 0.25),
        
        make_tuple(Tree::Root::LRC, VITreeM::Selection::WORST_FIT, 0.0),
        //make_tuple(Tree::Root::LRC, VITreeM::Selection::WORST_FIT, 0.50),
        //make_tuple(Tree::Root::LRC, VITreeM::Selection::WORST_FIT, 0.25),

        make_tuple(Tree::Root::CENTER, VITreeM::Selection::BEST_FIT, 0.0),
        //make_tuple(Tree::Root::CENTER, VITreeM::Selection::BEST_FIT, 0.50),
        //make_tuple(Tree::Root::CENTER, VITreeM::Selection::BEST_FIT, 0.25),

        make_tuple(Tree::Root::CENTER, VITreeM::Selection::WORST_FIT, 0.0),
        //make_tuple(Tree::Root::CENTER, VITreeM::Selection::WORST_FIT, 0.50),
        //make_tuple(Tree::Root::CENTER, VITreeM::Selection::WORST_FIT, 0.25),
    };

    vector<string> param_root = { "LRC | ", "CENTER | " };
    vector<string> param_selection = { "BEST_FIT | ", "WORST_FIT | " };
    vector<string> param_alpha = { "NORMAL", "ALPHA 25%", "ALPHA 50%"  };

    JSONObject curve;
    JSONArray points;

    JSONArray tcurves, ocurves, fcurves, acurves, nlcurves, llcurves;

    total = params.size() * events.size();
    current = 0;

    map<string, vector<PDD> > stats;
    for(auto param : params){
        
        string name = param_root[(int)get<0>(param)];
        name += param_selection[(int)get<1>(param)];
        name += param_alpha[(get<2>(param) == 0.0 ? 0 :(get<2>(param) == 0.25 ? 1 : 2))];

        curve[L"name"] = new JSONValue(to_wstring(name));

        cout << name << endl;
        auto stats = simulate(phy, vis, events, get<0>(param), get<1>(param), get<2>(param));

        // TIME MAPPING
        points.clear();
        for(PDD pdd : stats["time_mapping"]){
            JSONValue *x = new JSONValue((double)pdd.first);
            JSONValue *y = new JSONValue((double)pdd.second);
            JSONArray point = {x, y};
            points.push_back(new JSONValue(point));
        }
        curve[L"points"] = new JSONValue(points);
        tcurves.push_back( new JSONValue(curve) );

        // OVERCOST
        points.clear();
        for(PDD pdd : stats["overcost"]){
            JSONValue *x = new JSONValue(pdd.first);
            JSONValue *y = new JSONValue(pdd.second);
            JSONArray point = {x, y};
            points.push_back(new JSONValue(point));
        }
        curve[L"points"] = new JSONValue(points);
        ocurves.push_back( new JSONValue(curve) );

        // ACCEPTANCE
        points.clear();
        for(PDD pdd : stats["acceptance"]){
            JSONValue *x = new JSONValue(pdd.first);
            JSONValue *y = new JSONValue(pdd.second);
            JSONArray point = {x, y};
            points.push_back(new JSONValue(point));
        }
        curve[L"points"] = new JSONValue(points);
        acurves.push_back( new JSONValue(curve) );

        // FRAGMENTATION
        points.clear();
        for(PDD pdd : stats["fragmentation"]){
            JSONValue *x = new JSONValue(pdd.first);
            JSONValue *y = new JSONValue(pdd.second);
            JSONArray point = {x, y};
            points.push_back(new JSONValue(point));
        }
        curve[L"points"] = new JSONValue(points);
        fcurves.push_back( new JSONValue(curve) );

        // NODE LOAD
        points.clear();
        for(PDD pdd : stats["node_load"]){
            JSONValue *x = new JSONValue(pdd.first);
            JSONValue *y = new JSONValue(pdd.second);
            JSONArray point = {x, y};
            points.push_back(new JSONValue(point));
        }
        curve[L"points"] = new JSONValue(points);
        nlcurves.push_back( new JSONValue(curve) );

        // LINK LOAD
        points.clear();
        for(PDD pdd : stats["link_load"]){
            JSONValue *x = new JSONValue(pdd.first);
            JSONValue *y = new JSONValue(pdd.second);
            JSONArray point = {x, y};
            points.push_back(new JSONValue(point));
        }
        curve[L"points"] = new JSONValue(points);
        llcurves.push_back( new JSONValue(curve) );

    }

    jtime_mapping[L"curves"] = new JSONValue(tcurves);
    jovercost[L"curves"] = new JSONValue(ocurves);
    jacceptance[L"curves"] = new JSONValue(acurves);
    jfragmentation[L"curves"] = new JSONValue(fcurves);
    jnodeload[L"curves"] = new JSONValue(nlcurves);
    jlinkload[L"curves"] = new JSONValue(llcurves);

    JSONArray subgs = {
        new JSONValue(jtime_mapping),
        new JSONValue(jovercost),
        new JSONValue(jacceptance),
        new JSONValue(jfragmentation),
        new JSONValue(jnodeload),
        new JSONValue(jlinkload)
    };

    root[L"subgraphics"] = new JSONValue(subgs);

    wofstream file(out_filename);

    JSONValue *fstats = new JSONValue(root);

    file << fstats->Stringify();

    file.close();
}

JSONObject create_subgraphic(
    wstring title,
    wstring xlabel,
    wstring ylabel,
    PII xaxis,
    PII yaxis
){
    JSONObject out;
    out[L"title"] = new JSONValue(title.c_str());
    out[L"xlabel"] = new JSONValue(xlabel.c_str());
    out[L"ylabel"] = new JSONValue(ylabel.c_str());
    out[L"curves"] = new JSONValue(JSONArray());

    if(xaxis.first == -1 or yaxis.first == -1)
        return out;

    JSONArray axis = {
        new JSONValue((double)xaxis.first),
        new JSONValue((double)xaxis.second),
        new JSONValue((double)yaxis.first),
        new JSONValue((double)yaxis.second)
    };
    out[L"axis"] = new JSONValue(axis);
    return out;
}

map<string, vector<PDD> > Simulator::simulate(
    Graph phy,
    vector<Graph> vis,
    vector<Event> events,
    Tree::Root root_type,
    VITreeM::Selection selection_type,
    double alpha
){
    double x,y;
    PIManager pimanager(phy, root_type, selection_type, alpha);
    map<string, vector<PDD> > stats;

    int it = 0, perc = 0;

    stats.clear();
    for(Event event : events){
        perc = 100.0*double(it)/double(events.size());
        cout << "\r" << perc << "%" << flush;

        current++;
        it++;

        if(event.type == 1){
            pimanager.allocate(event.vid, vis[event.vid]);
        }else if(event.type == 0){
            pimanager.deallocate(event.vid, vis[event.vid]);
        }

        x = event.interval;

        y = pimanager.time_mapping();
        stats["time_mapping"].push_back(PDD(x, y));

        y = pimanager.overcost();
        stats["overcost"].push_back(PDD(x, y));

        y = pimanager.fragmentation();
        stats["fragmentation"].push_back(PDD(x, y));

        y = pimanager.acceptance();
        stats["acceptance"].push_back(PDD(x, y));

        y=pimanager.node_load();
        stats["node_load"].push_back(PDD(x,y));

        y=pimanager.link_load();
        stats["link_load"].push_back(PDD(x,y));

    }
    cout << endl;
    return stats;
}
