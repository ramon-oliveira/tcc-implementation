#ifndef __VI_TREE_M__
#define __VI_TREE_M__

#include "StdLib.h"

#include "Graph.h"
#include "Tree.h"
#include "Utils.h"

class VITreeM{
public:
    enum Selection{BEST_FIT, WORST_FIT};

	VITreeM(Tree _host, Tree _guest, Selection _selection_method, double _alpha);
	VITreeM();
    
    bool is_valid();
    friend ostream &operator<<(ostream &os, VITreeM &vitreem);

	Tree host;
	Tree guest;

	vector<int> nodes_map;
	map<int, map<int,VI> > edges_map;
private:

    bool is_matchable(int host_node, int guest_node);

    bool mapping(
        int p_host_node, int host_node, 
        int p_guest_node, int guest_node, 
        int carry_cost
    );

    bool step1(
        int p_host_node, int host_node, 
        int p_guest_node, int guest_node, 
        int carry_cost
    );

    bool step2(
        int p_host_node, int host_node, 
        int p_guest_node, int guest_node, 
        int carry_cost
    );

    VPII selection_order(int p_host_node, int p_guest_node, VPII selection);
    
    Selection selection_method;
    double alpha;
};

#endif
