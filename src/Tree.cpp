#include "Tree.h"

// converter_type (0, virtual) (1, physical)
// root_type (0, lrc) (1, root(0))
Tree::Tree(Graph _graph, Converter converter_type, Root root_type):
	graph(_graph),
	super_nodes(graph.nodes.size()),
    root(-1)
{

    assert(graph.nodes.size() > 0);
    // copies from graph
    cap_mtx = graph.cap_mtx;
	adj_list = graph.adj_list; 
    virtual_adj_list.assign(graph.nodes.size(), VI());

    assert(graph.connected());

    // convert graph to tree
    converter(converter_type);

	// build super nodes tree
	build();

    // define tree's root
    define_root(root_type);
}

Tree::Tree()
{}

VI Tree::path_btw(int a, int b){
    assert(a>=0 and a<(int)graph.nodes.size());
    assert(b>=0 and b<(int)graph.nodes.size());
    VI path;
    VI parent(graph.nodes.size(), -1);
    VI visited(graph.nodes.size(), false);
    queue<int> que;
    que.push(a);

    while(not que.empty()){
        int u = que.front(); 
        que.pop();
        visited[u] = true;
        for(int v : adj_list[u]){
            if(not visited[v]){
                parent[v] = u;
                que.push(v);
            }
        }
    }

    int p = b;
    while(parent[p] != -1){
        path.push_back(p);
        p = parent[p];
    }
    path.push_back(a);
    reverse(all(path));
    return path;
}

void Tree::converter(Converter converter_type){
    kruskal();

    if(converter_type == Tree::Converter::VIRTUAL){
        for(int u=0; u<(int)virtual_adj_list.size(); u++){
            for(int v : virtual_adj_list[u]){
                VI path = path_btw(u, v);
                for(int i=0; i< int(path.size()) - 1; i++){
                    int a = path[i];
                    int b = path[i+1];
                    cap_mtx[a][b] += cap_mtx[u][v];
                    cap_mtx[b][a] += cap_mtx[u][v];
                }
            }
        }
    }
}

bool krukal_cmp(Edge a, Edge b){
    return a.cap > b.cap;
}

int union_find(int v, VI &parent){
    if(parent[v] == v) return v;
    return parent[v] = union_find(parent[v], parent);
}

void Tree::kruskal(){
    vector< Edge > edges;
    for(int u=0; u<(int)graph.nodes.size(); u++){
        for(int v : adj_list[u]){
            if(u < v){
                edges.push_back(Edge(u, v, cap_mtx[u][v]));
            }
        }
    }
    sort(all(edges), krukal_cmp);

    VI parent(graph.nodes.size());
    for(int i=0; i<(int)graph.nodes.size(); i++)
        parent[i] = i;

    VVI new_adj_list(graph.nodes.size());
    for(Edge edge : edges){
        int u = union_find(edge.u, parent);
        int v = union_find(edge.v, parent);
        if(u != v){
            parent[u] = v;
            new_adj_list[edge.u].push_back(edge.v);
            new_adj_list[edge.v].push_back(edge.u);
        }else{
            virtual_adj_list[edge.u].push_back(edge.v);
            virtual_adj_list[edge.v].push_back(edge.u);
        }
    }

    adj_list = new_adj_list;
}

void Tree::define_root(Root root_type){
    if(root_type == Root::LRC)
        root_lrc();
    else if(root_type == Root::CENTER)
        root_center(); 
}

void Tree::root_lrc(){
    VPII cand;
    for(int u=0; u<(int)graph.nodes.size(); u++){
        int edge_cap_sum = 0;
        for(int v : adj_list[u]){
            edge_cap_sum += cap_mtx[u][v];
        }
        cand.push_back(PII(-graph.nodes[u].cap*edge_cap_sum, u));
    }
    sort(all(cand));
    assert((int)cand.size() > 0);
    bool found = false;
    for(PII c : cand){
        if(graph.nodes[c.second].type != 0){
            root = c.second;
            found = true;
            break;
        }
    }
    if(not found)
        root = cand[0].second;
}

void Tree::root_center(){
	vector< int > degree( graph.nodes.size(), 0);
	vector< bool > is_root( graph.nodes.size(), true);
	for(int i=0; i<(int)degree.size(); i++)
		degree[i] = adj_list[i].size();

	int alive = degree.size();
    VI type1;

	while(alive>2){
		vector<int> cut;
		for(int i=0; i<(int)degree.size(); i++){
			if( degree[i]==1 ){
				is_root[i] = false;
				cut.push_back(i);
				alive--;
                if(graph.nodes[i].type == 1)
                    type1.push_back(i);
			}
		}
		for(int v : cut){
			degree[v]=0;
			for(int u : adj_list[v]){
				if(degree[u]){
					degree[u]--;
				}
			}
		}
	}

	assert(alive<=2 && alive>0);
	
    vector<int> roots;
	for(int i=0; i<(int)graph.nodes.size(); i++){
		if(is_root[i])
			roots.push_back(i);
	}
	
	assert( alive == (int)roots.size());
    root = roots[0];
    if(graph.nodes[root].type == 0){
        if(roots.size() == 2 and graph.nodes[roots[1]].type == 1)
            root = roots[1];
        else if(type1.size() > 0)
            root = type1[type1.size()-1];
    }
}


void Tree::build(){
	for(Node &node : graph.nodes){
		//pnc = (pair node cap) or (pair neighbor cap)
        super_nodes[node.id][node.id] = SuperNode(node);
		for(int neighbor : adj_list[node.id]){
			if(super_nodes[node.id].count(neighbor) == 0){
				super_nodes[node.id][neighbor] = build_dfs(node.id, neighbor); 
			}

            SuperNode ret = super_nodes[node.id][neighbor];
            super_nodes[node.id][node.id] += ret;
            int edge_cap = cap_mtx[node.id][neighbor];
            super_nodes[node.id][node.id].edge_cap += edge_cap;
		}
	}
}

SuperNode Tree::build_dfs(int parent, int current){
	SuperNode out(graph.nodes[current]);

	for(int neighbor: adj_list[current]){
		if(neighbor != parent and super_nodes[current].count(neighbor) > 0){
			out += super_nodes[current][neighbor];
			out.edge_cap += cap_mtx[current][neighbor];
		}else if(neighbor != parent){
			super_nodes[current][neighbor] = build_dfs(current, neighbor);
			out += super_nodes[current][neighbor];
			out.edge_cap += cap_mtx[current][neighbor]; 
		}
	}

	return out;
}

SuperNode Tree::get_box(int from, int to){
    assert(from >=0 and from<(int)graph.nodes.size());
    assert(to >=0 and to<(int)graph.nodes.size());
    //assert(count(all(adj_list[from]), to) == 1);

    queue<int> Q;
    VI vis(graph.nodes.size(), false);
    vis[from] = true;
    SuperNode out;
    Q.push(to);
    while(not Q.empty()){
        int u = Q.front(); Q.pop();
        vis[u] = true;
        out.node_types_cap[graph.nodes[u].type] += graph.nodes[u].cap;
        for(int v : adj_list[u]){
            if(not vis[v]){
                out.edge_cap += cap_mtx[u][v];
                Q.push(v);
            }
        }
    }

    return out;
}

void Tree::add_to_edge(int a, int b, int value){
    cap_mtx[a][b] += value;
    assert(cap_mtx[a][b]>=0);

    cap_mtx[b][a] += value;
    assert(cap_mtx[b][a] >= 0);
}

ostream &operator<<(ostream &os, const Tree &t){
    os << "nodes\n";
    for(Node node : t.graph.nodes){
        os << node << "\n";
    }
    os << "root: " << t.root << "\n";

    os << "\nsuper nodes\n";
    for(int u=0; u<(int)t.super_nodes.size(); u++){
        os << u << " = [";
        bool f = true;
        for(pair<int, SuperNode> pis : t.super_nodes[u]){
            if(!f)
                os << ", ";
            f = false;
            os << pis.first << " " << pis.second; 
        }
        os << "]\n";
    }

    os << "\nedges\n";
    for(int u=0; u<(int)t.graph.nodes.size(); u++){
        for(int v : t.adj_list[u]) if(u < v){
            os << "(" << u << ", " << v << ") = " << t.cap_mtx[u][v] << "\n";
        }
    }

    os << "\nvirtual edges\n";
    for(int u=0; u<(int)t.graph.nodes.size(); u++){
        for(int v : t.virtual_adj_list[u]) if(u < v){
            os << "(" << u << ", " << v << ") = " << t.cap_mtx[u][v] << "\n";
        }
    }

    return os;
}
