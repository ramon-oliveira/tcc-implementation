#ifndef __STDLIB__
#define __STDLIB__

#include <cstdio>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstring>
#include <string>
#include <sstream>

#include <algorithm>
#include <exception>
#include <map>
#include <set>
#include <queue>
#include <utility>
#include <vector>
#include <string>
#include <cassert>
#include <tuple>
#include <random>

using namespace std;
#define INF (1<<30)
typedef pair<int,int> PII;
typedef pair<double, double> PDD;

typedef vector<PII> VPII;
typedef vector<VPII> VVPII;

typedef vector<int> VI;
typedef vector<VI> VVI;


#define all(a) a.begin(), a.end()
#define INF (1<<30)

#endif
