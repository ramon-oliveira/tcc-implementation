#ifndef __GRAPH_GEN__
#define __GRAPH_GEN__

#include "StdLib.h"
#include "Graph.h"

class GraphGen{
public:
    virtual Graph generate() = 0;
};

#endif
