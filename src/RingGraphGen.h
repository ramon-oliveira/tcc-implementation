#ifndef __RING_GRAPH_GEN__
#define __RING_GRAPH_GEN__

#include "StdLib.h"
#include "Utils.h"
#include "Graph.h"
#include "GraphGen.h"

class RingGraphGen: public GraphGen{
public:	
	struct Config{
		PII n_nodes;
		PII node_cap;
		PII edge_cap;
		Config(PII n_nodes, PII node_cap, PII edge_cap):
            n_nodes(n_nodes), 
            node_cap(node_cap),
            edge_cap(edge_cap)
        {}
	};

    RingGraphGen(Config config);

	Graph generate();

    Config config;
};

#endif
