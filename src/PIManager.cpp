#include "PIManager.h"

PIManager::PIManager(
    Graph _phy, 
    Tree::Root _root_type, 
    VITreeM::Selection _selection_method,
    double _alpha
):
    phy(_phy),
    root_type(_root_type),
    selection_method(_selection_method),
    original_phy(phy),
    vi_count(0),
    vi_accepted(0),
    total_time_mapping(0.0),
    total_overcost(0.0),
    alpha(_alpha)
{}

bool PIManager::allocate(int vid, Graph vi){
    assert(mappings.count(vid) == 0);

    vi_count++;

    double tini = stime();

    VITreeM mapping(
            Tree(phy, Tree::PHYSICAL, root_type), 
            Tree(vi, Tree::VIRTUAL, root_type), 
            selection_method, alpha
    ); 

    double tend = stime();

    total_time_mapping += (tend - tini);
    
    if(mapping.is_valid()){
        mappings[vid] = mapping;
        phy = mapping.host.graph;
        vi_accepted++;
        // calc overcost here
        double o = 0;
        double t = 0;
        for(int u = 0; u<(int)vi.nodes.size(); u++){
            for(int v : vi.adj_list[u]){
                if(u > v)
                    continue;
                int edge_cap = vi.cap_mtx[u][v];
                t += edge_cap;
                o += (int(mapping.edges_map[u][v].size()) - 1)*edge_cap;
            }
        }
        total_overcost += o/t;
    }

    return mapping.is_valid();
}

bool PIManager::deallocate(int vid, Graph vi){
    if(mappings.count(vid) == 0)
        return false;
    VITreeM mapping = mappings[vid];

    for(int v=0; v<(int)vi.nodes.size(); v++){
        int u = mapping.nodes_map[v];
        if(not (u>=0 and u<(int)phy.nodes.size()))
            cout << "u: " << u << endl;
        assert(u>=0 and u<(int)phy.nodes.size());
        assert(v>=0 and v<(int)vi.nodes.size());
        phy.nodes[u].cap += vi.nodes[v].cap;
    }

    for(int u=0; u<(int)vi.nodes.size(); u++){
        for(int v : vi.adj_list[u]){
            if(u > v)
                continue;
            int cap = vi.cap_mtx[u][v];
            VI path = mapping.edges_map[u][v];
            for(int i=0; i<(int)path.size() - 1; i++){
                int a = path[i];
                int b = path[i+1];
                phy.cap_mtx[a][b] += cap;
                phy.cap_mtx[b][a] += cap;
            }
        }
    }

    return true;
}


/* Metrics */
double PIManager::fragmentation(){
    int active = 0;
    int total = 0;
    
    //nodes
    for(int i=0; i<(int)phy.nodes.size(); i++){
        active += (original_phy.nodes[i].cap > phy.nodes[i].cap);
    }
    total += phy.nodes.size();
    
    //edges
    for(int u=0; u<(int)phy.adj_list.size(); u++){
        for(int v : phy.adj_list[u]){
            if(u > v)
                continue;
            
            total++;
            active += (original_phy.cap_mtx[u][v] > phy.cap_mtx[u][v]);
        }
    }
    
    return 100.0*double(active)/double(total);
}

double PIManager::time_mapping(){
    return total_time_mapping/double(vi_count);
}

double PIManager::acceptance(){
    return 100.0*double(vi_accepted)/double(vi_count);
}

double PIManager::overcost(){
    return double(total_overcost)/double(vi_count);
}

double PIManager::node_load(){
    double load = 0;
    double total = 0;
    
    //nodes
    for(int u=0; u<(int)phy.nodes.size(); u++) if(phy.nodes[u].type==1){
        bool tor = false;
        for(int v : phy.adj_list[u])
            if(phy.nodes[v].type == 0)
                tor = true;

        if(tor){
            load += original_phy.nodes[u].cap - phy.nodes[u].cap;
            total += original_phy.nodes[u].cap;
        }
    }
	return 100.0*double(load)/double(total);
}

double PIManager::link_load(){
	//edges
	double total=0, load=0;
    for(int u=0; u<(int)phy.adj_list.size(); u++){
        for(int v : phy.adj_list[u]){
            if(u > v)
                continue;
            
            total += original_phy.cap_mtx[u][v];
            load += original_phy.cap_mtx[u][v] - phy.cap_mtx[u][v];
        }
    }
	return 100.0*load/total;
}

