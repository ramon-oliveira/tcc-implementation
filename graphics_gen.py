#!/usr/bin/env python
# coding: utf-8

import matplotlib.pyplot as pyplot
import json
import sys

dictionary = {
    "Acceptance": u"Aceitação",
    "Mapping time": u"Tempo de mapeamento",
    "Fragmentation": u"Fragmentação",
    "Channel load": u"Carga dos canais",
    "Interval": u"Intervalo",
    "interval": u"Intervalo",
    "CENTER": "CENTRO",
}


def translate(txt):
    for d in dictionary:
        if d in txt:
            return txt.replace(d, dictionary[d])
    return txt


markers = ['.', 'o', '^', '1', 's', 'p', '*', '+', 'x', 'D', 'h', ',']

colors = [
    (0, 255, 0),
    (0, 0, 255),
    (255, 0, 0),
    (80, 80, 80),
    (0, 0, 255),
    (192, 192, 192),
    (128, 128, 128),
    (128, 0, 0),
    (128, 128, 0),
    (0, 128, 0),
    (128, 0, 128),
    (0, 128, 128),
    (0, 0, 128)
]

colors = [(1.0*r/255, 1.0*g/255, 1.0*b/255) for r, g, b in colors]




def graphics_gen(jgraphic, filename):
    """Graphic generator."""
    pyplot.figure(figsize=(20, 20))

    description = jgraphic["description"]
    pyplot.suptitle(description)

    nplots = len(jgraphic["subgraphics"])
    curplot = 1
    for subgraphic in jgraphic["subgraphics"]:
        if "ver" in subgraphic["title"]:
            continue
        pyplot.subplot(nplots//2 + nplots % 2, 2, curplot)
        #box = ax.get_position()
        #ax.set_position([box.x0, box.y0, box.width*0.8, box.height])
        title = translate(subgraphic["title"])
        xlabel = translate(subgraphic["xlabel"])
        ylabel = translate(subgraphic["ylabel"])

        pyplot.title(title, fontsize=25)
        pyplot.xlabel(xlabel, fontsize=25)
        pyplot.ylabel(ylabel, fontsize=25)
        if "axis" in subgraphic:
            pyplot.axis(subgraphic["axis"])

        cnt = 0
        for i, curve in enumerate(subgraphic["curves"]):
            name = curve["name"]
            if r"%" in name:
                continue
            cnt += 1
            name = name[:-8]
            points = curve["points"]
            xs = [x for x, y in points]
            ys = [y for x, y in points]
            pyplot.plot(xs, ys, label=translate(name), color=colors[cnt-1],
                        marker=markers[i], linewidth=2.0, markevery=5,
                        markeredgewidth=1)

        loc = "upper left"
        if "Acc" in subgraphic["title"]:
            loc = "lower left"
        pyplot.legend(loc=loc, fontsize=15)

        curplot += 1

    pyplot.savefig(filename)

if __name__ == "__main__":
    if len(sys.argv) < 3:
            print 'usage graphics_gen <infilename> <outfilename>'
    else:
        infilename = sys.argv[1]
        outfilename = sys.argv[2]
        with open(infilename, 'r') as f:
            gplot = json.loads(f.read())
            graphics_gen(gplot, outfilename)
